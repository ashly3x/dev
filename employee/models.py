from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Employe(models.Model):
    fullname = models.CharField(max_length=100)
    emp_code = models.CharField(max_length=3)
    mobile = models.CharField(max_length=15)
    skills = ArrayField(models.CharField(max_length=200), blank=True)