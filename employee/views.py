from django.shortcuts import render
from . import models
import requests
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from . import serializers
# Create your views here.

class EmployeeDet(APIView):
    def post(self, request):
        try:
            data = request.data
            edu = models.Employe()
            edu.fullname = data["fullname"]
            edu.emp_code = data["emp_code"]
            edu.mobile = data["mobile"]
            edu.skills = data["skills"]
            edu.save()
            return Response(ResponseReturn.successfn({"Detail": "Record Added Successfully"}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        try:
            edu = models.Employe.objects.all()
            arr = []
            for ed in edu:
                for skill in ed.skills:
                    arr.append(skill)
           # serializer = serializers.EmployeeSerializer(educationdatamodel, many=True)
            newl = set(arr)
            
            return Response(ResponseReturn.successfn({"msg":newl}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"status": "Not Found" + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request):
        try:
            data = request.data
            edu = models.Employe.objects.get(id=data["id"])
            edu.fullname = data["fullname"]
            edu.emp_code = data["emp_code"]
            edu.mobile = data["mobile"]
            edu.skills = data["skills"]
            edu.save()
            return Response(ResponseReturn.successfn({"Detail": "Record Updated Successfully"}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, request):
        try:
            data = request.data
            edu = models.Employe.objects.get(id=data["id"])
            edu.delete()
            return Response(ResponseReturn.successfn({"Detail": "Record Deleted Successfully"}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)


class EmployeeDelt(APIView):
    def get(self, request):
        try:
            data = request.data
            edu = models.Employe.objects.all()
            for ed in edu:
                if ed.id != 2:
                    ed.delete()
            return Response(ResponseReturn.successfn({"Detail": "Record Deleted Successfully"}), status = status.HTTP_200_OK)
        except Exception as e:
            return Response(ResponseReturn.failurefn({"Detail": "Issue " + str(e)}), status = status.HTTP_500_INTERNAL_SERVER_ERROR)


class ResponseReturn:
    def successfn(data):
        res = {}
        res["type"] = "Success"
        res["data"] = data
        return res

    def failurefn(data):
        res = {}
        res["type"] = "Fail"
        res["data"] = data
        return res